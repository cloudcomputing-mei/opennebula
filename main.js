#! /usr/bin/env node
require('dotenv').config();

var commands = require('./lib/commands');

switch (process.argv.slice(2)[0]) {
    case "tmpl": commands.templates(); break;
    case "vm": commands.vms(); break;
    case "host": commands.hosts(); break;
    case "img": commands.images(); break;
    case "help": commands.help(); break;
    default: console.log("Command not found. Try: `cc help`"); break;
}

// ONE_HOST=http://192.168.1.240:2633/RPC2
// ONE_CREDENTIALS=oneadmin:d82a2be86f29a3b5e663323986f4a358