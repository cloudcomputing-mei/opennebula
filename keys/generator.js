var keypair = require('keypair'),
  fs = require('fs');

module.exports = (name) => {
  let pair = keypair();

  fs.writeFile('./keys/' + name + '_public.pem', pair.public, err => {
    if (err) return console.log(err);
  });

  fs.writeFile('./keys/' + name + '_private.pem', pair.private, err => {
    if (err) return console.log(err);
  });

  return pair.public;
}
