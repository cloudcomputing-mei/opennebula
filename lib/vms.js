var OpenNebula = require('opennebula'),
    one = new OpenNebula(process.env.ONE_CREDENTIALS, process.env.ONE_HOST),
    rl = require('readline').createInterface({ input: process.stdin, output: process.stdout });

//GET all VMs
exports.list = () => {
    one.getVMs((err, data) => {
        if (err) console.error(err);
        else data.forEach(vm => {
            if (parseInt(vm.STATE) >= 3 && parseInt(vm.LCM_STATE) === 3) {
                console.log("\x1b[34mID\x1b[0m: ", vm.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", vm.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[32m         RUNNING\x1b[0m");
            } else if (parseInt(vm.STATE) === 8 && parseInt(vm.LCM_STATE) === 0) {
                console.log("\x1b[34mID\x1b[0m: ", vm.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", vm.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m         STOPPED\x1b[0m");
            } else if (parseInt(vm.STATE) === 5 && parseInt(vm.LCM_STATE) === 0) {
                console.log("\x1b[34mID\x1b[0m: ", vm.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", vm.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[32m          PAUSED\x1b[0m");
            } else if (parseInt(vm.STATE) >= 3 && parseInt(vm.LCM_STATE) === 18) {
                console.log("\x1b[34mID\x1b[0m: ", vm.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", vm.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[35m        POWEROFF\x1b[0m");
            } else if (parseInt(vm.STATE) >= 3 && parseInt(vm.LCM_STATE) === 38) {
                console.log("\x1b[34mID\x1b[0m: ", vm.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", vm.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m MIGRATE_FAILURE\x1b[0m");
            } else {
                console.log("\x1b[34mID\x1b[0m: ", vm.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", vm.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[35m         PENDING\x1b[0m");
            }
        });
        process.exit(1);
    });
}

//GET VM information
exports.getInfo = (id) => {
    one.getVM(parseInt(id)).info((err, vm) => {
        if (err) console.error(err);
        else {
            if (parseInt(vm.VM.STATE) >= 3 && parseInt(vm.VM.LCM_STATE) === 3) {
                console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), vm.VM.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), vm.VM.NAME, "\n\x1b[34m- state\x1b[0m:\x1b[32m         RUNNING\x1b[0m");
            } else if (parseInt(vm.VM.STATE) === 8 && parseInt(vm.VM.LCM_STATE) === 0) {
                console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), vm.VM.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), vm.VM.NAME, "\n\x1b[34m- state\x1b[0m:\x1b[31m         STOPPED\x1b[0m");
            } else if (parseInt(vm.VM.STATE) === 5 && parseInt(vm.VM.LCM_STATE) === 0) {
                console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), vm.VM.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), vm.VM.NAME, "\n\x1b[34m- state\x1b[0m:\x1b[32m          PAUSED\x1b[0m");
            } else if (parseInt(vm.VM.STATE) >= 3 && parseInt(vm.VM.LCM_STATE) === 18) {
                console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), vm.VM.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), vm.VM.NAME, "\n\x1b[34m- state\x1b[0m:\x1b[31m        POWEROFF\x1b[0m");
            } else if (parseInt(vm.VM.STATE) >= 3 && parseInt(vm.VM.LCM_STATE) === 38) {
                console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), vm.VM.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), vm.VM.NAME, "\n\x1b[34m- state\x1b[0m:\x1b[31m MIGRATE_FAILURE\x1b[0m");
            } else {
                console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), vm.VM.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), vm.VM.NAME, "\n\x1b[34m- state\x1b[0m:\x1b[35m         PENDING\x1b[0m");
            }
            console.log("\x1b[34m- resources\x1b[0m", "cpu: ".padStart(10), vm.VM.TEMPLATE.CPU);
            console.log("memory: ".padStart(22), vm.VM.TEMPLATE.MEMORY + " mb");
            console.log("\x1b[34m- usage    \x1b[0m", "cpu: ".padStart(10), Math.round(vm.VM.MONITORING.CPU / vm.VM.TEMPLATE.CPU * 100) + "%");
            console.log("memory: ".padStart(22), Math.round((vm.VM.MONITORING.MEMORY * 0.001) / vm.VM.TEMPLATE.MEMORY * 100) + "%");
            console.log("\x1b[34m- disk     \x1b[0m", "image: ".padStart(10), vm.VM.TEMPLATE.DISK.IMAGE);
            if (vm.VM.TEMPLATE.CONTEXT.NETWORK === 'YES') {
                console.log("\x1b[34m- network  \x1b[0m", "ip: ".padStart(10), vm.VM.TEMPLATE.NIC.IP);
                console.log("mac: ".padStart(22), vm.VM.TEMPLATE.NIC.MAC);
                console.log("rx: ".padStart(22), Math.round(vm.VM.MONITORING.NETRX * 0.001) + " kb");
                console.log("tx: ".padStart(22), Math.round(vm.VM.MONITORING.NETTX * 0.001) + " kb");
            }
            if (vm.VM.HISTORY_RECORDS) {
                console.log("\x1b[34m- host     \x1b[0m", "id: ".padStart(10), vm.VM.HISTORY_RECORDS.HISTORY.HID);
                console.log("name: ".padStart(22), vm.VM.HISTORY_RECORDS.HISTORY.HOSTNAME);
            }
        }
        process.exit(1);
    });
}

//CREATE VM
exports.create = () => {
    rl.question('name: ', (answer) => {
        let vm_name = answer !== "" ? answer : require('uuid/v4')();

        one.getHosts((err, data) => {
            if (err) console.error(err);
            else {
                data.forEach(host => {
                    switch (host.STATE) {
                        case "0": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[35m     INIT\x1b[0m"); break;
                        case "2": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[32m       OK\x1b[0m"); break;
                        case "3": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m    ERROR\x1b[0m"); break;
                        case "4": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m DISABLED\x1b[0m"); break;
                        case "8": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m      OFF\x1b[0m"); break;
                        default: break;
                    }
                });
                rl.question('host (id): ', (answer) => {
                    let host_id = answer !== "" ? parseInt(answer) : 0;
                    rl.question('use template (y/n): ', (answer) => {
                        if (answer === "y") one.getTemplates((err, tmpls) => {
                            if (err) console.error(err);
                            else {
                                tmpls.forEach(tmpl => console.log("\x1b[34mID\x1b[0m: ", tmpl.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", tmpl.NAME));
                                rl.question('template (id): ', (answer) => {
                                    let template_id = answer !== "" ? parseInt(answer) : 0;
                                    one.getTemplate(template_id).instantiate(vm_name, undefined, undefined, function (err, vm) {
                                        if (err) console.error(err);
                                        else one.getVM(parseInt(vm.id)).deploy(host_id, false, 0, function (err, data) {
                                            if (err) console.error(err);
                                            else console.log("\x1b[32mMachine up.\x1b[0m ID: ", data);
                                            process.exit(1);
                                        });
                                    });
                                    rl.close();
                                });
                            }
                        });
                        else {
                            rl.question('memory (mb): ', (vm_memory) =>
                                rl.question('VCPU (cores): ', (vm_vcpu) =>
                                    rl.question('cpu: ', (vm_cpu) =>
                                        rl.question("description: ", (template_description) => {
                                            let obj = {
                                                description: template_description !== "" ? template_description : "",
                                                image: "ttylinux",
                                                network: "cloud",
                                                memory: vm_memory !== "" ? vm_memory : "128",
                                                vcpu: vm_vcpu !== "" ? vm_vcpu : "1",
                                                cpu: vm_cpu !== "" ? vm_cpu : "0.1"
                                            };

                                            rl.question("save template (y/n): ", (answer) => {
                                                if (answer === "y") rl.question("template name: ", (template_name) => {

                                                    template_name = template_name !== "" ? template_name : require('uuid/v4')();
                                                    let ssh_key = require(__dirname + '/../keys/generator')(template_name);
                                                    one.createTemplate('NAME = ' + template_name + '\nVCPU = ' + obj.vcpu + ' \nCPU = ' + obj.cpu + '\nDESCRIPTION = "' + obj.description + '"\nDISK = [IMAGE = ' + obj.image + ' ]\nMEMORY = ' + obj.memory + ' \nNIC = [ NETWORK = ' + obj.network + ']\nCONTEXT=[NETWORK="YES", SSH_PUBLIC_KEY="' + ssh_key + '"]', function (err, data) {
                                                        if (err) console.log(err);
                                                        else one.getTemplate(parseInt(data.id)).instantiate(vm_name, undefined, undefined, function (err, vm) {
                                                            if (err) console.log(err);
                                                            else one.getVM(parseInt(vm.id)).deploy(host_id, false, 0, function (err, data) {
                                                                if (err) console.error(err);
                                                                else console.log("\x1b[32mMachine up.\x1b[0m ID: ", data);
                                                                process.exit(1);
                                                            });
                                                        });
                                                    });
                                                    rl.close();
                                                });
                                                else {
                                                    let ssh_key = require(__dirname + '/../keys/generator')(vm_name);
                                                    one.getTemplate(0).instantiate(vm_name, undefined, '\nVCPU = ' + obj.vcpu + '\nCPU = ' + obj.cpu + '\nDESCRIPTION = "' + obj.description + '"\nDISK = [IMAGE = ' + obj.image + ' ] \nMEMORY = ' + obj.memory + ' \nNIC = [ NETWORK = ' + obj.network + ']\nCONTEXT=[NETWORK="YES", SSH_PUBLIC_KEY="' + ssh_key + '"]', function (err, vm) {
                                                        if (err) console.log(err);
                                                        else one.getVM(parseInt(vm.id)).deploy(host_id, false, 0, function (err, data) {
                                                            if (err) console.error(err);
                                                            else console.log("\x1b[32mMachine up.\x1b[0m ID: ", data);
                                                            process.exit(1);
                                                        });
                                                    });
                                                    rl.close();
                                                }
                                            });
                                        }))));
                        }
                    });
                });
            }
        });
    });
}

//Reboot VM
exports.reboot = (id) => {
    var vm = one.getVM(parseInt(id));
    rl.question('hard (y/n): ', (answer) => {
        if (answer === "y") vm.action('reboot-hard', function (err) {
            if (err) console.error(err);
            else console.log("\x1b[32m(Hard) Reboot successfully done.\x1b[0m");
            process.exit(1);
        });
        else vm.action('reboot', function (err) {
            if (err) console.error(err);
            else console.log("\x1b[32mReboot successfully done.\x1b[0m");
            process.exit(1);
        });
        rl.close();
    })
}

//Poweroff VM
exports.powerOff = (id) => {
    var vm = one.getVM(parseInt(id));
    rl.question('hard (y/n): ', (answer) => {
        if (answer === "y") vm.action('poweroff-hard', function (err) {
            if (err) console.error(err);
            else console.log("\x1b[32m(Hard) Poweroff successfully done.\x1b[0m");
            process.exit(1);
        });
        else vm.action('poweroff', function (err) {
            if (err) console.error(err);
            else console.log("\x1b[32mPoweroff successfully done.\x1b[0m");
            process.exit(1);
        });
        rl.close();
    });
}

//DELETE VM
exports.delete = (id) => {
    var vm = one.getVM(parseInt(id));
    vm.action('delete', function (err) {
        if (err) console.error(err);
        else console.log("\x1b[32mVirtual machine successfully deleted.\x1b[0m");
        process.exit(1);
    });
}

//Migrate VM
exports.migrate = (id) => {
    var vm = one.getVM(parseInt(id));
    one.getHosts((err, data) => {
        if (err) console.error(err);
        else {
            data.forEach(host => {
                switch (host.STATE) {
                    case "0": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[35m     INIT\x1b[0m"); break;
                    case "2": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[32m       OK\x1b[0m"); break;
                    case "3": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m    ERROR\x1b[0m"); break;
                    case "4": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m DISABLED\x1b[0m"); break;
                    case "8": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m      OFF\x1b[0m"); break;
                    default: break;
                }
            });
            rl.question('host (id): ', (host) => {
                host = host !== "" ? parseInt(host) : 0;
                rl.question('live migrate (y/n): ', (live) => {
                    live = live === "y" ? true : false;
                    vm.migrate(host, live, false, 0, function (err) {
                        if (err) console.error(err);
                        else console.log("\x1b[32mVirtual machine successfully migrated.\x1b[0m");
                        process.exit(1);
                    })
                    rl.close();
                });
            });
        }
    });
}

exports.help = () => {
    console.log("Usage: cc vm <command>\n")
    console.log("  where <command> is one of:")
    console.log("    list".padEnd(20), "List all virtual machines");
    console.log("    info <vm_id>".padEnd(20), "Get info about virtual machine");
    console.log("    create".padEnd(20), "Create a virtual machine");
    console.log("    reboot <vm_id>".padEnd(20), "Reboot a virtual machine");
    console.log("    poweroff <vm_id>".padEnd(20), "Poweroff a virtual machine");
    console.log("    start  <vm_id>".padEnd(20), "Start a virtual machine");
    console.log("    delete <vm_id>".padEnd(20), "Delete a virtual machine");
    console.log("    migrate <vm_id>".padEnd(20), "Migrate a virtual machine to another host");
    process.exit(1);
}

exports.start = (id) => {
    one.getVM(parseInt(id)).action(
        "resume", function (err, data){
            if (err) console.error(err);
            else console.log("\x1b[32mMachine up.\x1b[0m");
            process.exit(1);
        }
    )
}