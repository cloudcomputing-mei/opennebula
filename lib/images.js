var OpenNebula = require('opennebula'),
  one = new OpenNebula(process.env.ONE_CREDENTIALS, process.env.ONE_HOST);

//get images
exports.list = () => {
  one.getImages((err, data) => {
    if (err) console.error(err);
    else data.forEach(img => console.log("\x1b[34mID\x1b[0m: ", img.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", img.NAME));
    process.exit(1);
  });
}

//get image info
exports.getInfo = (id) => {
  one.getImage(parseInt(id)).info((err, img) => {
    if (err) console.error(err);
    else {
      let types = ["OS", "CDROM", "DATABLOCK", "KERNEL", "RAMDISK", "CONTEXT"];
      console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), img.IMAGE.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), img.IMAGE.NAME)
      console.log("\x1b[34m- owner \x1b[0m", "id: ".padStart(6), img.IMAGE.UID);
      console.log("name: ".padStart(15), img.IMAGE.UNAME);
      console.log("\x1b[34m- disk  \x1b[0m", "size: ".padStart(5), img.IMAGE.SIZE + " mb");
      console.log("type: ".padStart(15), types[parseInt(img.IMAGE.TYPE)]);
      console.log("\x1b[34m- origin\x1b[0m", "path: ".padStart(5), img.IMAGE.PATH);
    }
    process.exit(1);
  });
}

exports.help = () => {
  console.log("Usage: cc img <command>\n")
  console.log("  where <command> is one of:")
  console.log("    list".padEnd(20), "List all images");
  console.log("    info <img_id>".padEnd(20), "Get info about an image");
  process.exit(1);
}