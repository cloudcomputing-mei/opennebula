exports.showHelpCommands = () => {
    console.log("Usage: cc <command>\n")
    console.log("  where <command> is one of:")
    console.log("    vm".padEnd(10), "Manage virtual machines");
    console.log("    tmpl".padEnd(10), "Manage templates");
    console.log("    host".padEnd(10), "Manage hosts");
    console.log("    img".padEnd(10), "Manage images");
}