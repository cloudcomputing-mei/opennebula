var OpenNebula = require('opennebula'),
    one = new OpenNebula(process.env.ONE_CREDENTIALS, process.env.ONE_HOST);

//get templates
exports.list = () => {
    one.getTemplates((err, tmpls) => {
        if (err) console.error(err);
        else tmpls.forEach(tmpl => console.log("\x1b[34mID\x1b[0m: ", tmpl.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", tmpl.NAME));
        process.exit(1);
    });
}

//get tmplate info
exports.getInfo = (id) => {
    one.getTemplate(parseInt(id)).info((err, tmpl) => {
        if (err) console.error(err);
        else {
            console.log("\x1b[34m- ID\x1b[0m: ".padEnd(20), tmpl.VMTEMPLATE.ID, "\n\x1b[34m- name\x1b[0m: ".padEnd(21), tmpl.VMTEMPLATE.NAME);
            console.log("\x1b[34m- owner    \x1b[0m", "id: ".padStart(10), tmpl.VMTEMPLATE.UID);
            console.log("name: ".padStart(22), tmpl.VMTEMPLATE.UNAME);
            console.log("\x1b[34m- resources\x1b[0m", "cpu: ".padStart(10), tmpl.VMTEMPLATE.TEMPLATE.CPU);
            console.log("memory: ".padStart(22), tmpl.VMTEMPLATE.TEMPLATE.MEMORY + " mb");
            console.log("\x1b[34m- disk     \x1b[0m", "image: ".padStart(10), tmpl.VMTEMPLATE.TEMPLATE.DISK.IMAGE);
        }
        process.exit(1);
    });
}

//delete template
exports.delete = (id) => {
    one.getTemplate(parseInt(id)).delete((err, tmpl) => {
        if (err) console.log(err);
        else console.log("\x1b[32mTemplate successfully deleted.\x1b[0m");
        process.exit(1);
    });
 }

 exports.help = () => {
    console.log("Usage: cc tmpl <command>\n")
    console.log("  where <command> is one of:")
    console.log("    list".padEnd(20), "List all templates");
    console.log("    info <tmpl_id>".padEnd(20), "Get info about template");
    console.log("    delete <tmpl_id>".padEnd(20), "Delete a template");
    process.exit(1);
}