var OpenNebula = require('opennebula'),
    one = new OpenNebula(process.env.ONE_CREDENTIALS, process.env.ONE_HOST),
    rl = require('readline').createInterface({ input: process.stdin, output: process.stdout });

//get hosts
exports.list = () => {
    one.getHosts((err, data) => {
        if (err) console.error(err);
        else data.forEach(host => {
            switch (host.STATE) {
                case "0": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[35m     INIT\x1b[0m"); break;
                case "2": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[32m       OK\x1b[0m"); break;
                case "3": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m    ERROR\x1b[0m"); break;
                case "4": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m DISABLED\x1b[0m"); break;
                case "8": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m      OFF\x1b[0m"); break;
                default: break;
            }
        });
        process.exit(1);
    });
}

// get host info
exports.getInfo = (id) => {
    one.getHost(parseInt(id)).info((err, host) => {
        if (err) console.error(err);
        else switch (host.HOST.STATE) {
            case "0": console.log("\x1b[34mID\x1b[0m: ", host.HOST.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.HOST.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[35m     INIT\x1b[0m"); break;
            case "2":
                console.log("\x1b[34m- ID\x1b[0m: ", host.HOST.ID.padStart(4), "\n\x1b[34m- name\x1b[0m: ", host.HOST.NAME.padStart(12), "\n\x1b[34m- state\x1b[0m:\x1b[32m       OK\x1b[0m");
                console.log("\x1b[34m- cluster   \x1b[0m", "id: ".padStart(9), host.HOST.CLUSTER_ID);
                console.log("name: ".padStart(22), host.HOST.CLUSTER);
                console.log("\x1b[34m- hypervisor\x1b[0m:", "name: ".padStart(8), host.HOST.TEMPLATE.HYPERVISOR);
                console.log("\x1b[34m- usage     \x1b[0m", "cpu: ".padStart(9), host.HOST.HOST_SHARE.CPU_USAGE + " / " + host.HOST.HOST_SHARE.MAX_CPU + " (" + Math.round(host.HOST.HOST_SHARE.CPU_USAGE / host.HOST.HOST_SHARE.MAX_CPU * 100) + "%)");
                console.log("memory: ".padStart(22), Math.round(host.HOST.HOST_SHARE.MEM_USAGE * 0.001) + " /" + Math.round(host.HOST.HOST_SHARE.MAX_MEM * 0.001) + " mb (" + Math.round(host.HOST.HOST_SHARE.MEM_USAGE / host.HOST.HOST_SHARE.MAX_MEM * 100) + "%)");
                console.log("disk: ".padStart(22), Math.round(host.HOST.HOST_SHARE.DISK_USAGE * 0.001) + " /" + Math.round(host.HOST.HOST_SHARE.MAX_DISK * 0.001) + " mb (" + Math.round(host.HOST.HOST_SHARE.DISK_USAGE / host.HOST.HOST_SHARE.MAX_DISK * 100) + "%)");
                break;
            case "3": console.log("\x1b[34mID\x1b[0m: ", host.HOST.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.HOST.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m    ERROR\x1b[0m"); break;
            case "4": console.log("\x1b[34mID\x1b[0m: ", host.HOST.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.HOST.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m DISABLED\x1b[0m"); break;
            case "8": console.log("\x1b[34mID\x1b[0m: ", host.HOST.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.HOST.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m      OFF\x1b[0m"); break;
            default: break;
        }
        process.exit(1);
    });
}

exports.create = () => {
    rl.question('name: ', (hostname) => {
        one.createHost(hostname, "kvm", "kvm", 0, 0, (err, host) => {
            if (err) console.error(err);
            else console.log("\x1b[32mHost created.\x1b[0m ID: ", host.id);
            process.exit(1);
        });
        rl.close();
    });
}

//delete template
exports.delete = (id) => {
    one.getHost(parseInt(id)).delete(err => {
        if (err) console.log(err)
        else console.log("\x1b[32mHost successfully deleted.\x1b[0m");
        process.exit(1);
    });
}

exports.migrateAll = (inHost, outHost) => {
    one.getHosts((err, data) => {
        if (err) console.error(err);
        else {
            data.forEach(host => {
                switch (host.STATE) {
                    case "0": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[35m     INIT\x1b[0m"); break;
                    case "2": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[32m       OK\x1b[0m"); break;
                    case "3": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m    ERROR\x1b[0m"); break;
                    case "4": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m DISABLED\x1b[0m"); break;
                    case "8": console.log("\x1b[34mID\x1b[0m: ", host.ID.padEnd(10), "\x1b[34mname\x1b[0m: ", host.NAME.padEnd(20), "\x1b[34mstate\x1b[0m:\x1b[31m      OFF\x1b[0m"); break;
                    default: break;
                }
            });
            rl.question('origin host (id): ', (inHost) => {
                rl.question('destination host (id): ', (outHost) => {
                    one.getVMs(function (err, data) {
                        if (err) console.error(err);
                        else data.forEach(vm => {
                            if (vm.HISTORY_RECORDS.HISTORY.HID === inHost) {
                                one.getVM(parseInt(vm.ID)).migrate(parseInt(outHost), false, false, 0, function (err, data) {
                                    if (err) console.error(err);
                                    else console.log("\x1b[32mMachines successfully migrated.\x1b[0m");
                                    process.exit(1);
                                })
                            }
                        });
                    });
                    rl.close();
                })
            });
        }
    });
}

exports.help = () => {
    console.log("Usage: cc host <command>\n")
    console.log("  where <command> is one of:")
    console.log("    list".padEnd(20), "List all hosts");
    console.log("    info <host_id>".padEnd(20), "Get info about host");
    console.log("    create".padEnd(20), "Create a host");
    console.log("    delete <host_id>".padEnd(20), "Delete a host");
    console.log("    migrate".padEnd(20), "Migrate all virtual machines from the origin host to destination host");
    process.exit(1);
}