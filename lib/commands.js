
exports.vms = () => {
  var vms = require('./vms');
  switch (process.argv.slice(2)[1]) {
    case "info": vms.getInfo(process.argv.slice(2)[2]); break;
    case "list": vms.list(); break;
    case "create": vms.create(); break;
    case "reboot": vms.reboot(process.argv.slice(2)[2]); break;
    case "poweroff": vms.powerOff(process.argv.slice(2)[2]); break;
    case "delete": vms.delete(process.argv.slice(2)[2]); break;
    case "migrate": vms.migrate(process.argv.slice(2)[2]); break;
    case "help": vms.help();break;
    case "start": vms.start(process.argv.slice(2)[2]); break;
    default: console.log("Command not found");process.exit(1); break;
  }
}

exports.templates = () => {
  var templates = require('./templates');
  switch (process.argv.slice(2)[1]) {
    case "info": templates.getInfo(process.argv.slice(2)[2]); break;
    case "list": templates.list(); break;
    case "delete": templates.delete(process.argv.slice(2)[2]); break;
    case "help": templates.help();break;
    default: console.log("Command not found");process.exit(1); break;
  }
}

exports.hosts = () => {
  var hosts = require('./hosts');
  switch (process.argv.slice(2)[1]) {
    case "info": hosts.getInfo(process.argv.slice(2)[2]); break;
    case "list": hosts.list(); break;
    case "create": hosts.create(); break;
    case "delete": hosts.delete(process.argv.slice(2)[2]); break;
    case "migrate": hosts.migrateAll(); break;
    case "help": hosts.help(); break;
    default:console.log("Command not found");process.exit(1); break;
  }
}

exports.images = () => {
  var images = require('./images');
  switch (process.argv.slice(2)[1]) {
    case "info": images.getInfo(process.argv.slice(2)[2]); break;
    case "list": images.list(); break;
    case "help": images.help();break;
    default:console.log("Command not found"); process.exit(1); break;
  }
}

exports.help = () => {
  var help = require('./help');
  help.showHelpCommands();
}